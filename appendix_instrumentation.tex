% !TEX root = .\Thesis.tex

% ================================================================================================================================
% Beginning of Instrumentation Appendix
% ================================================================================================================================

% ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
% Overview
% ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
\section{Overview}\label{sec:instr_overview}

Different parts of the engine were instrumented in order to numerically quantity what occurs during CSAPong games. Various metrics (e.g. particles actually emitted) were examined, and various sections of code were timed. These two use cases were separated into two application systems that I created that resided within the engine itself, the \class{Metrics} and \class{Timing} systems. As mentioned before, these systems will save a series of files containing the gathered information into a timestamped folder within CSAPong's save data file system. These files fueled the direction of these studies due to their ability to answer questions about the engine such as "What function in \class{Particle Effect Component} takes the longest to run?"

% ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
% Metrics System
% ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
\section{Metrics System}\label{sec:instr_metrics_system}

% ----------------------------------------------------------------------------------------------------------------------------
\subsection{Metadata}\label{ssec:instr_metrics_system_metadata}
% ----------------------------------------------------------------------------------------------------------------------------

\begin{figure}
\addtocounter{figure}{-1}
	\addsubfigure{instrumentation_metrics_system_metadata_example}
	{0.5}{1.0}
	{An example of metadata output by the \class{Metrics System}.}
	{An example of metadata output by the \class{Metrics System}. The important part to understand here is that these games are fall under the multiple-burst scenario. In other words, the particle effect emits its particles over time. As \fig{fig:MetricsSystemMetadataGameExample} illustrates, the particles per emission (PPE) is always 10\% of the effect's total maximum particles (TMP) because the step values (PPE step is 50, TMP step is 500) are what dictates the amount is added to PPE and TMP for each game as time goes on.}
	{fig:MetricsSystemMetadataExample}
	%
	\addsubfigure{instrumentation_metrics_system_metadata_game_example}
	{0.5}{1.0}
	{An example of the changing particle values during games in CSAPong.}
	{An example of the changing particle values during games in CSAPong. This shows a series of games that fall under the multiple-burst scenario with a min/max/step of 500/2000/500 and with particles per emission (PPE) emitting 10\% of the effect's total maximum particles (TMP).}
	{fig:MetricsSystemMetadataGameExample}
\end{figure}


Although not structurally complicated, the \class{Metrics System} keeps track of a great deal of primitives that describe the series of games run by CSAPong. The following is a list of the tracked primitives that remained constant throughout the games:

\begin{singlespace}

	\begin{itemize}[nolistsep,topsep=5pt]
	\itemsep1.0em

		\item \define{Are particles looping}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item Whether or not the particle effect is looping. This means that the particle effect will continue even after the duration has elapsed.
		\end{itemize}

		\item \define{Number of particle effects}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item The number of \class{Particle Effect Components} that are attached to the ball entity.
		\end{itemize}

		\item \define{Is total maximum particles changing}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item Whether or not \idefine{total maximum particles} is changing across different particle effect types.
		\end{itemize}

		\item \define{Total maximum particles} (or TMP)
		\begin{itemize}[nolistsep,topsep=0pt]
			\item If \idefine{is total maximum particles changing} is false, then this will be the constant value of \idefine{total maximum particles} across particle effect types.
		\end{itemize}

		\item \define{Is particles per emission changing}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item Whether or not \idefine{particles per emission} is changing across different particle effect types.
		\end{itemize}

		\item \define{Particles per emission} (or PPE)
		\begin{itemize}[nolistsep,topsep=0pt]
			\item If \idefine{is particles per emission changing} is false, then this will be the constant value of \idefine{particles per emission} across particle effect types.
		\end{itemize}

		\item \define{Minimum particles}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item The minimum value for the changing values (i.e. \idefine{total maximum particles} and/or \idefine{particles per emission}).
		\end{itemize}

		\item \define{Maximum particles}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item The maximum value for the changing values (i.e. \idefine{total maximum particles} and/or \idefine{particles per emission}).
		\end{itemize}

		\item \define{Particles step}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item The step value for the changing values (i.e. \idefine{total maximum particles} and/or \idefine{particles per emission}). This is used in conjunction with the minimum and maximum particles values to determine how many particle effects to create and use during CSAPong's execution.
		\end{itemize}

		\item \define{Particles per emission step}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item If \idefine{is particles per emission changing} is true, then this will dictate the step value for \idefine{particles per emission} across particle effect types. This is generally a fraction of \idefine{particles step}.
		\end{itemize}

		\item \define{Total maximum particles step}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item If \idefine{is total maximum particles changing} is true, then this will dictate the step value for \idefine{total maximum particles} across particle effect types. This is generally a fraction of \idefine{particles step}.
		\end{itemize}

		\item \define{Total number of runs}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item The number of games to run per particle effect type.
		\end{itemize}

		\item \define{Duration per run}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item The amount of time in seconds that a single game lasts.
		\end{itemize}

	\end{itemize}

\end{singlespace}

\fig{fig:MetricsSystemMetadataExample} can be examined in order to make the distinction clear between the different particle variables. The \idefine{minimum particles} is 500, the \idefine{maximum particles} is 2000, and the \idefine{particles step} is 500. This tells us right away that there will be $ 2000 / 500 = 4 $ particle definition files that will be created and played\footnotemark. Note that this is similar to the example shown near the end of \ch{ch:csapong_game} which also had a min/max/step of 500/2000/500.

From the figure, we also know that \idefine{particles per emission} and \idefine{total maximum particles} are changing over time and they will not be constant values. Thus, their "step" values will tell us how much each variable (i.e. total maximum particles and particles per emission) will be added to as the game advances. We can also see that the \idefine{total maximum particles step} is 500, or 100\% of the \idefine{particles step}, and the \idefine{particles per emission step} is 50, or 10\% of the \idefine{particles step}. This is the same as the example variable blues given for the multiple-burst scenario in \ch{ch:csapong_game}.

\fig{fig:MetricsSystemMetadataGameExample} shows how all of these particle variables change as CSAPong executes. Note that, since there are 4 different kinds of particle effects and 2 \idefine{total number of runs}, CSAPong would play a total of $ 4 \times 2 = 8 $ times in this example.

\footnotetext{Note that we do not subtract 500 from 2000 since the range is inclusive.}

% ----------------------------------------------------------------------------------------------------------------------------
\subsection{Metrics}\label{ssec:instr_metrics_system_metrics}
% ----------------------------------------------------------------------------------------------------------------------------

\addbottomfigure{instrumentation_metrics_system_metrics_example}
{1.0}
{An example of metrics output by the \class{Metrics System} based on the metadata from \fig{fig:MetricsSystemMetadataExample}.}
{An example of metrics output by the \class{Metrics System} based on the metadata from \fig{fig:MetricsSystemMetadataExample}. }
{fig:MetricsSystemMetricsExample}


The \class{Metrics System} keeps track of three engine metrics, but it also manages six system metrics. Engine metrics are metrics that originate from the engine, e.g. counting the number of times that each particle was drawn. System metrics are metrics that come from the metrics system, e.g. the current game run number. In other words, system metrics come from metadata values, but they are printed with the engine metrics in order to give them context. The following list shows all of the different engine and system metrics that are tracked:

\begin{singlespace}

	\begin{itemize}[nolistsep,topsep=0pt]
	\itemsep1.0em

		\item \define{Engine Metrics}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item \define{All effects particles actually emitted}
				\begin{itemize}[nolistsep,topsep=0pt]
					\item As the game progresses, the size of the \var{New Particle Indices} array after each emission is added to this metric.
				\end{itemize}
			\item \define{All effects particles actually rendered}
				\begin{itemize}[nolistsep,topsep=0pt]
					\item This is incremented every time a particle is rendered in the \class{Drawable} instance.
				\end{itemize}
			\item \define{Render ball called}
				\begin{itemize}[nolistsep,topsep=0pt]
					\item This is incremented every time the ball is rendered.
				\end{itemize}
		\end{itemize}

		\item \define{System Metrics}
		\begin{itemize}[nolistsep,topsep=0pt]
			\item \define{Run number}
			\item \define{Per effect total max particles}
			\item \define{Per effect particles per emission}
			\item \define{All effects total max particles}
			\item \define{All effects particles per emission}
		\end{itemize}

	\end{itemize}

\end{singlespace}

% ----------------------------------------------------------------------------------------------------------------------------
\subsection{Particle Effect Definition}\label{ssec:instr_metrics_pe_definition}
% ----------------------------------------------------------------------------------------------------------------------------

As mentioned in \ch{ch:background}, \class{Particle Effects} that are passed into \class{Particle Effect Components} cannot be created programmatically. They must be created with JSON particle definition files. If tens or hundreds of particle effects needed to be created, then just as many particle definition files would have to be made. This is clearly something that should be done by hand, and so a script, \func{generate\_particles.py}, was created to automate this process. An example invocation of this script is shown in \clis{code:InvokeGenerateParticles}, and that invocation will generate the particle definition files shown in \fig{fig:GenerateParticleFilesListing}. Note that these generated particle effects are the same ones that were used in \fig{fig:MetricsSystemMetadataExample}.

\addfigure{instrumentation_generate_particles_py}
{0.8}{Shows the parameters that the \func{generate\_particles.py} script can use.}
{Shows the parameters that the \func{generate\_particles.py} script can use.}
{fig:GenerateParticleFilesScript}

\framecode{bash}{metrics}{generate_particles.sh}
{code:InvokeGenerateParticles}
{An example invocation of the generate particles script.}
\vspace{-2.0cm}
\addfigure{instrumentation_generate_particles_files}
{1.0}
{Shows the particle files that were generated by an invocation like in \clis{code:InvokeGenerateParticles}.}
{Shows the particle files that were generated by an invocation like in \clis{code:InvokeGenerateParticles}.}
{fig:GenerateParticleFilesListing}


% ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
% Timing System
% ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
\section{Timing System}\label{sec:instr_timing_system}

% ----------------------------------------------------------------------------------------------------------------------------
\subsection{Visual Studio}\label{ssec:instr_timing_system_vs}
% ----------------------------------------------------------------------------------------------------------------------------

\begin{figure}
\addtocounter{figure}{-1}

	\vspace{-1.5cm}
	\addsubfigure{instrumentation_visual_studio_call_tree}
	{1.0}{1.0}
	{Shows an example of Visual Studio's generated call tree for CPU instrumentation when ran using the metadata from \fig{fig:MetricsSystemMetadataExample}.}
	{Shows an example of Visual Studio's generated call tree for CPU instrumentation when ran using the metadata from \fig{fig:MetricsSystemMetadataExample}. This call tree in particular shows the "hot path" of execution, or the path that leads to the "function leaf" with the highest exclusive samples \%. In this instance, the function calls that led to \func{ParticleDrawable::DrawParticles} was the "hot path" of execution.}
	{fig:VisualStudioCallTree}

	\addsubfigure{instrumentation_visual_studio_call_tree_sorted_filtered}
	{1.0}{1.0}
	{Shows the same call tree from \fig{fig:VisualStudioCallTree}, but sorted and filtered.}
	{Shows the same call tree from \fig{fig:VisualStudioCallTree}, but sorted and filtered. The function call tree level is $ \geq 8 $, the function names begin with "ChilliSource::", the inclusive samples \% is $ \geq 6\% $, and it is sorted by the inclusive samples \% in descending order. These parameters were used to ensure that the results were low-level ChilliSource function calls with a high inclusive sample percent. The functions with the most inclusive samples are either related to rendering or updating particles.}
	{fig:VisualStudioCallTreeSorted}
\end{figure}

ChilliSource generates a Visual Studio solution file for the developer to use when a project is created, and so Visual Studio was utilized as an IDE during development on a Windows machine. Visual Studio boasts a wide array of profiling tools which include (but is certainly not limited to) CPU sampling and function instrumentation. The CPU sampling was considerably helpful during the initial stages of these studies. The inclusive (includes samples from all functions calls within it) and exclusive (only includes samples from itself) sampling pointed out hot spots within the \class{Particle Effect Component} to investigate. The call trees that the sampling produced (see figures \fig{fig:VisualStudioCallTree} and \fig{fig:VisualStudioCallTreeSorted}) also assisted in understanding how the engine worked. Although all of this information is helpful, the CPU sampling does not provide concrete times. The function instrumentation that Visual Studio provided should have filled that gap, but it was not used simply because it did not reliably and smoothly work. It would slow down the game considerably during execution, and then would take a great deal of time to process after execution. Even if it did work, it did not have the flexibility to instrument custom sections, and it also could not be used to instrument the game on other platforms such as iOS or Android.

% ----------------------------------------------------------------------------------------------------------------------------
\subsection{Shiny}\label{ssec:instr_timing_system_shiny}
% ----------------------------------------------------------------------------------------------------------------------------

Seeking a way to instrument functions and custom blocks of code across all platforms, a third party library called Shiny was used. Shiny is an older C/C++/Lua intrusive profiler created by Aidin Abedi with "very very low overhead" \cite{AbediShinyProfiler}. It allows the user to insert macros within functions and code blocks, and it outputs a call tree very similar to Visual Studio's with the time used by the named code blocks and function calls. Although Shiny's creator asserted that Shiny is "amazingly simple to use and flexible", it took about a week to integrate Shiny within CSAPong and ChilliSource in such a way that it worked on Windows, iOS, and Android. The majority of the encountered problems had more to do with compilation errors and flags, however, than the usage of Shiny's API. The API of Shiny is actually simple and flexible to use, as shown in \clis{code:ParticleUpdateShiny}. Regrettably, Shiny has two major flaws. First, it does not reliably run if the program is multi-threaded (and ChilliSource is multi-threaded). Second, the output is not easily parsed since it is not separated by delimiters, and parsable output was required to quickly format, collate, and examine results during this thesis.

\framecode{C++}{metrics}{ParticleUpdateShiny.cpp}
{code:ParticleUpdateShiny}
{Using the pseudocode from \clis{code:ParticleUpdate}, this shows how \class{Shiny} could be used to instrument \func{ParticleUpdateTask}. }

\vspace{-0.5cm}
% ----------------------------------------------------------------------------------------------------------------------------
\subsection{Timing Application System}\label{ssec:instr_timing_system_custom}
% ----------------------------------------------------------------------------------------------------------------------------

\addtopfigure{instrumentation_timing_system_timing_exampe_no_inner}
{0.6}
{An example of timing output by the \class{Timing System} with just the \func{ParticleUpdateTask} and the \idefine{Particle Iteration} times.}
{An example of timing output by the \class{Timing System} with just the \func{ParticleUpdateTask} and the \idefine{Particle Iteration} times.}
{fig:TimingSystemTimingExampleNoInner}

I developed the \class{Timing System} due to dissatisfaction with the Visual Studio and Shiny function profilers. This system reliably instruments the multi-threaded engine across all platforms and returns results that are easily parsed. It uses the built-in \class{Performance Timer} from ChilliSource along with its own \func{StartTimer} and \func{StopTimer} static methods in order to time code blocks in a similar fashion to Shiny, as shown in \clis{code:ParticleUpdateTimingSystem}. To achieve this, it uses three hash tables\footnotemark $ $ with the following key-value pairs:

\footnotetext{In C++ 11, the closest thing to a hash table is an unordered map from the std namespace.}

\begin{singlespace}

	\begin{itemize}[nolistsep,topsep=0pt]
	\itemsep1.0em

		\item \var{Timing Hash Table}
			\begin{itemize}[nolistsep,topsep=0pt]
				\item KEY:  (std::string) Code section name
				\item VALUE: (double) Total time
			\end{itemize}

		\item \var{Timer Hash Table}
			\begin{itemize}[nolistsep,topsep=0pt]
				\item KEY:  (std::string) Timer key
				\item VALUE: (ChilliSource::PerformanceTimer) Timer object
			\end{itemize}

		\item \var{Counting Hash Table}
			\begin{itemize}[nolistsep,topsep=0pt]
				\item KEY:  (std::string) Code section name
				\item VALUE: (unsigned int) Number of code section calls
			\end{itemize}

	\end{itemize}

\end{singlespace}

\framecode{C++}{metrics}{ParticleUpdateTimingSystem.cpp}
{code:ParticleUpdateTimingSystem}
{Using the pseudocode from \clis{code:ParticleUpdate}, this shows how our custom \class{Timing System} could be used to instrument \func{ParticleUpdateTask}. }


% ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
% Output and its Evolution
% ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
\section{Output and its Evolution}\label{sec:instr_output}

From the examples that were shown above, it can be observed that the system output lends itself to a tabular structure that follows a comma-separated-values format. Storing the data like this was simple, but storing the metadata proved to be more difficult. 

At first, the metadata was stored all within the titles of the CSV (e.g. metrics\_maxRunNum = 5\_particle-effects = 1 ... [timestamp].csv). This worked well when there were only a couple of metadata values, but, as shown earlier in this chapter, there are now a great deal of metadata values. Predictably, the titles of the CSVs eventually became so unruly that Windows refused to open the files because the "file path was too long". This problem was solved by first bundling the metrics and timing CSVs into one timestamped directory, and then outputting another CSV file exclusively for metadata in that same directory. 

Although the metadata problem was solved, bundles of CSVs were not a particularly tidy approach when it came to distribution. However, any other method of outputting the data in C++ would be far too complicated and out of scope for the purposes of our studies. Outputting the data in another format using python, however, would not be as complex. I created a python script to import the three bundled CSVs into an HDF5 file. HDF5, i.e. Hierarchical Data Format 5, is "a unique open source technology suite for managing data collections of all sizes and complexity,"\cite{HDFGroupHDF5Introduction}, designed to support large, complex datasets that could be used on every size and type of system. HDF5 is hierarchical, high-performance, portable, can be used in numerous languages, and it is self-describing. Admittedly, HDF5 may be overkill for my tens of CSVs, but its ability to efficiently bundle all of the output files with metadata built right in is still useful. The HDF5 file that contains all of the results presented in this thesis can be found in a Bitbucket repository \cite{GrossCSThesisDataset}.


% ================================================================================================================================
% End of Instrumentation Appendix
% ================================================================================================================================
